import { NetSpectPage } from './app.po';

describe('net-spect App', () => {
  let page: NetSpectPage;

  beforeEach(() => {
    page = new NetSpectPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
