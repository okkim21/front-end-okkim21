import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Data } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { QplComponent } from './qpl.component';
import { QplService } from 'app/qpl/qpl.service';
import { QplFilterPipe } from './qpl-filter.pipe';
import { PaginationComponent } from '..//share/pagination/pagination.component';

describe('QplComponent', () => {
  let component: QplComponent;
  let fixture: ComponentFixture<QplComponent>;
  let element: HTMLElement;

  beforeEach(async(() => {
    const QplServiceMock = {};

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      declarations: [
        QplComponent,
        PaginationComponent,
        QplFilterPipe
      ],
      providers: [
        {
            provide: ActivatedRoute,
            useValue: {
                snapshot: {
                    data: {
                        data: [{
                          id: 1,
                          partNumber: '01086',
                          revision: 'DE',
                          partName: 'Russell67',
                          toolDieSetNumber: '30934',
                          isQualified: false,
                          openPo: false,
                          jurisdiction: null,
                          classification: null,
                          supplierName: 'Acme Corporation',
                          supplierCodes: '',
                          ctq: true,
                          lastUpdatedBy: 'Jorge.Goodman',
                          lastUpdatedDateUtc: '2017-09-23T07:37:41.9',
                          qplExpirationDate: null,
                          uppapStatus: null,
                          uppapExpiresUtc: null,
                          uppapUpdatedBy: null,
                          uppapUpdatedDateUtc: null
                        }],
                    }
                },
                data: {
                  subscribe: (fn: (value: Data) => void) => fn({
                      page: 1,
                      size: 20
                  })
              }
            }
        },
        { provide: QplService, useValue: QplServiceMock },
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QplComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('initial display', () => {
    it('should have created the correct title', () => {
        // Arrange
        const expectedTitle = 'View QPL List';

        // Act
        component.ngOnInit();
        fixture.detectChanges();

        // Assert
        expect(element.querySelector('.title').textContent).toContain(expectedTitle);
    });
});
});
