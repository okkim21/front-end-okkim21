import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'

import { Observable } from 'rxjs/Observable';

import { QplService } from './qpl.service';

@Injectable()
export class QplResolverService implements Resolve<any[]> {

  constructor(private qplService: QplService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any[]> {
    let page = +route.params['page'];
    const size = +route.params['size'];
    page -= 1;
    return this.qplService.getQplList(page, size);
  }

}
