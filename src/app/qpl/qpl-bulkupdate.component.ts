import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router'
import { Observable } from 'rxjs/Observable';

import { QplService } from './qpl.service';

@Component({
  selector: 'app-qpl-bulkupdate',
  templateUrl: './qpl-bulkupdate.component.html',
  styleUrls: ['./qpl-bulkupdate.component.scss']
})

export class QplBulkupdateComponent implements OnInit {
  @Input() partList : string[];

  bulkForm: FormGroup;

  partNumberList: string[]= []

  constructor( private qplService: QplService, private router: Router, public activeModal: NgbActiveModal, private fb: FormBuilder) { }

  ngOnInit() {
    this.partNumberList = this.partList;

    this.bulkForm = this.fb.group({
      qplFlag: '',
      expirationDate: '',
      status: '',
      uppapExpirationDate: ''
    });
  }


  saveBulk() {
    if (this.bulkForm.dirty && this.bulkForm.valid) {
      let qualified : boolean = false;
      if (this.bulkForm.get('qplFlag').value ==="Yes") {
        qualified = true;
     
      }

      let data = JSON.stringify({
        'qplIds': this.partList,
        'isQualified': qualified,
        'qplExpirationDate': this.bulkForm.get('expirationDate').value,
        'uppapStatus': this.bulkForm.get('status').value,
        'uppaExpiresUtc': this.bulkForm.get('uppapExpirationDate').value
      });

      this.qplService.updateQpl(data).subscribe(
        ()=>this.onSaveComplete(),
        (error: any) => console.log(<any>error)
      );

    }
  }

  onSaveComplete() {
    this.bulkForm.reset();
    this.activeModal.close();
  }
}
