
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { DataService } from '../share/data.service';
import { IQpl } from './qpl';

@Injectable()
export class QplService {
  private baseUrl = 'qpl?';
  constructor(private dataService: DataService) { }

  getQplList(offset: number, pageSize: number): Observable<any[]> {
     const url = `${this.baseUrl}` + 'offset=' + `${offset}&` + 'pageSize=' + `${pageSize}`;

     return this.dataService.httpGetList(url);
  }

  updateQpl(bulkInfo: any) {
    let url = 'qpl'
    return this.dataService.httpUpdate(bulkInfo, url);
  }
}
