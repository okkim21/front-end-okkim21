import { Pipe, PipeTransform } from '@angular/core';

import { IQpl } from './qpl';

@Pipe({
  name: 'qplFilter'
})
export class QplFilterPipe implements PipeTransform {
  // filtering
  transform(value: IQpl[], filterBy: string): IQpl[] {
    filterBy = filterBy ? filterBy.toLocaleLowerCase() : null ;
    return filterBy ? value.filter((qpl: IQpl) => {
        if (qpl.partName === null || qpl.partName === '') {
          return qpl.lastUpdatedBy.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
          qpl.supplierName.toLocaleLowerCase().indexOf(filterBy) !== -1
        } else {
          return qpl.partName.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
          qpl.lastUpdatedBy.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
          qpl.supplierName.toLocaleLowerCase().indexOf(filterBy) !== -1
        }
      }) : value ;
  }

}
