import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Observable } from 'rxjs/Observable';

import { QplBulkupdateComponent } from './qpl-bulkupdate.component';
import { QplService } from './qpl.service';
import { IQpl } from './qpl';

@Component({
  selector: 'app-qpl',
  templateUrl: './qpl.component.html',
  styleUrls: ['./qpl.component.scss']
})

export class QplComponent implements OnInit {
  qplList: IQpl[];
  page = 1;
  limit = 10;
  total = 100;
  currentPartNumber;


  errorMessage: string;
  listFilter: string;

  constructor(private qplService: QplService, private route: ActivatedRoute, 
    private router: Router, private modalService: NgbModal) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.getQplList(data['data']);
    })
  }

    
selectRow(event: any, item: any) {
    
        this.currentPartNumber = item.partMumber;
}
  getQplList(data: any[]) {
    this.qplList = data;
   for(var i = 0; i< this.qplList.length; i++) {
    this.qplList[i].itemCheck = false;

   }
  }

  onPrev(): void {
    this.page--;
    const prevPage = this.page;
    this.router.navigate(['/qpl', 'page', prevPage, 'size', this.limit]);
  }

  onNext(): void {
    this.page++;
    const nextPage = this.page;
    this.router.navigate(['/qpl', 'page', nextPage, 'size', this.limit]);
  }

  onFirst(): void {
    this.page = 1;
    this.router.navigate(['/qpl', 'page', this.page, 'size', this.limit]);
  }

  onLast(value: number): void {
    this.page = value;
   this.router.navigate(['/qpl', 'page', this.page, 'size', this.limit]);
  }

  changePagesize(value: number): void {
    this.page = 1;
    this.limit = value;
    this.router.navigate(['/qpl', 'page', this.page, 'size', this.limit]);
  }

  // bulk update
  showBulkUpdate(): void {


   let qplNumberList : any[] = [];
   let len = this.qplList.length;

   for (var i = 0; i<len; i++) {
     if (this.qplList[i].itemCheck){
      qplNumberList.push(this.qplList[i].id);
     }
   }

   if (qplNumberList.length > 0) {
    const modalRef = this.modalService.open(QplBulkupdateComponent, { size: 'lg' });
    modalRef.componentInstance.partList = qplNumberList;

    modalRef.result.then((result) => {
      this.router.navigate(['/qpl', 'page', this.page, 'size', this.limit]);
    });
   } else {
     console.log('No selection!');
   }
  }
}
