import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QplComponent } from './qpl/qpl.component';
import { QplResolverService } from './qpl/qpl-resolver.service';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'qpl/page/1/size/10'},
    { path: 'qpl/page/:page/size/:size',
      component: QplComponent,
      resolve : {data : QplResolverService }
    }
  ];

  @NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
