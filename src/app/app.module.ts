import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { QplComponent } from './qpl/qpl.component';
import { PaginationComponent } from './share/pagination/pagination.component';

import { DataService } from './share/data.service';
import { QplResolverService } from './qpl/qpl-resolver.service';
import { QplService } from './qpl/qpl.service';
import { QplFilterPipe } from './qpl/qpl-filter.pipe';
import { QplBulkupdateComponent } from './qpl/qpl-bulkupdate.component';

@NgModule({
  declarations: [
    AppComponent,
    QplComponent,
    PaginationComponent,
    QplFilterPipe,
    QplBulkupdateComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    DataService,
    QplResolverService,
    QplService
  ],
  entryComponents: [QplBulkupdateComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
