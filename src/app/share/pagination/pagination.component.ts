import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  // get value from other component
  @Input() page: number;
  @Input() count: number;
  @Input() pageSize: number;

  // emit data for other component
  @Output() goPrev = new EventEmitter<boolean>();
  @Output() goNext = new EventEmitter<boolean>();
  @Output() goFirst = new EventEmitter<boolean>();
  @Output() goLast = new EventEmitter<number>();
  @Output() changePagesize = new EventEmitter<number>();

  paginationForm: FormGroup;
  // set page size
  pageSizeList: any[] = [
    {size: 10},
    {size: 25},
    {size: 50}];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.paginationForm = this.fb.group({
      pageSize: this.pageSize
    });
    //  when page size changes
    this.paginationForm.get('pageSize').valueChanges
                         .subscribe(value => this.onPageSize(value));
  }

  onPageSize(value: number): void {
    this.changePagesize.emit(value);
  }

  getMin(): number {
    return ((this.pageSize * this.page) - this.pageSize) + 1;
  }

  getMax(): number {
    let max = this.pageSize * this.page;
    if (max > this.count) {
      max = this.count;
    }
    return max;
  }

  onPrev(): void {
    this.goPrev.emit(true);
  }

  onNext(next: boolean): void {
    this.goNext.emit(next);
  }

  onFirst(): void {
    this.goFirst.emit(true);
  }

  onLast(next: boolean): void {
    const lastPage: number = Math.ceil(this.count / this.pageSize);
    this.goLast.emit(lastPage);
  }

  lastPage(): boolean {
    return this.pageSize * this.page >= this.count;
  }

}
