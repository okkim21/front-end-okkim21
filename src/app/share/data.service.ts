
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {
  private baseUrl = 'https://exam.net-inspect.com';

  constructor(private http: Http) { }

  setHttpHeader(): any {
    const token = '992FBB06-F635-46E8-ADFC-F2498B22D050';
    const headers = new Headers({ 'Authorization': 'Key ' + token, 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return options;
  }

  httpGetList(urlString: string): Observable<any[]> {
    const url = `${this.baseUrl}/${urlString}`;
    const options = this.setHttpHeader();

    return this.http.get(url, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  httpUpdate(data: any, urlString: string) {
    const url = `${this.baseUrl}/${urlString}`;
    const options = this.setHttpHeader();

    return this.http.put(url, data, options)
    .map(this.extractData)
    .catch(this.handleError);
  }

  private extractData(response: Response) {
    const body = response.json();
    return body || {};
  }

  private handleError(error: Response): Observable<any> {
    return Observable.throw(error.json().error || 'Server error');
  }

}
