import { Observable } from 'rxjs/Rx';
import { DataService } from './data.service';

describe('DataService', () => {
    let dataService: DataService;
    const baseUrl = 'https://exam.net-inspect.com'
    let httpMock;
    beforeEach(() => {
        httpMock = jasmine.createSpyObj('httpMock', ['get']);
        dataService = new DataService(httpMock);
    });

    describe('httpGetList', () => {
        it ('should call http.get with the right url', () => {
            // Arrange
            const url = 'qpl?offset=0&pageSize=20';
            const expectedFullUrl = `${baseUrl}/${url}` ;
            httpMock.get.and.returnValue(Observable.of(<any[]>[]));

            // Act
            const result = dataService.httpGetList(url);

            // Assert
            expect(httpMock.get).toHaveBeenCalledWith(expectedFullUrl, jasmine.any(Object));
        });
    });
});
