// These are some default gulp tasks configured for your convenience
// Please feel free to add tasks or modify these

var gulp = require("gulp"),
    typescript = require('gulp-typescript'),
    sass = require("gulp-sass");

// Watch for SCSS and TS changes and build
gulp.task("default", function() {
    gulp.watch(["src/scss/**/*.scss", "app/**/*.scss"], ["sass"]);
    gulp.watch(["src/ts/**/*.ts"], ["typescript"]);
});

// Build SCSS
gulp.task("sass", function() {
    return gulp.src(["src/scss/**/*.scss"], { base: "" })
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest("./dist/css"));
});

// Build TS
gulp.task("typescript", function() {
    return gulp.src(["src/**/*.ts"], { base: "" })
		.pipe(typescript())
		.pipe(gulp.dest("./dist/js"));
});