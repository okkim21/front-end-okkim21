Net-Inspect: Qualified Parts
======
Setup
------
1. Clone repository
   > git clone https://bitbucket.org/okkim21/front-end-okkim21.git

2. Install dependencies
   > npm install

3. Install @angular/cli 
   ([github](https://github.com/angular/angular-cli), [npm](https://www.npmjs.com/package/@angular/cli))
   > npm install -g @angular/cli

Run
------
1. Run NG Live Development Server
   > ng serve

2. Browse [localhost:4200](http://localhost:4200)

3. Unit Test 

    You can see three unit tests running.
    - Whether data service hit the expected API endpoint.
    - Whether the QPL component is loaded and the corresponding template is loaded.
   > ng test 